import React from 'react';
import {Layout, List, ListItem, Text, TopNavigation, Divider, TopNavigationAction, Icon} from '@ui-kitten/components';
import {SafeAreaView} from 'react-native';
import 'react-native-gesture-handler';

const BackIcon = (style) => (
    <Icon {...style} name='arrow-back' />
);

class CollegeScreen extends React.Component {

    constructor(props) {
        super(props);
        this.handleBack = this.handleBack.bind(this);
    }

    handleBack() {
        this.props.navigation.goBack();
    }

    renderNavigateBack() {
        return (
            <TopNavigationAction icon={BackIcon} onPress={this.handleBack}/>
        );
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <TopNavigation title="College" alignmment="center" leftControl={this.renderNavigateBack()} />
                <Divider/>
                <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text category='h1'>Colleges</Text>
                    <List data={this.props.route.params.colleges.Colleges}
                          renderItem={({item,index}) => {
                              return (
                                  <ListItem title={item.CollegeName}/>
                              )
                          }}/>
                </Layout>
            </SafeAreaView>
        );
    }
}

export default CollegeScreen;